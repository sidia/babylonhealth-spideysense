package project.sitemap;

import org.junit.Test;
import project.configuration.ConfigValidator;
import project.crawler.dto.PageWithLink;
import project.crawler.SiteManager;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

public class SiteMapBuilderTest {
    @Test
    public void testSiteMapBuilder() throws IOException {
        SiteManager siteManager = new SiteManager();
        TestConfig configuration = new TestConfig(new ConfigValidator());
        String testFile = System.getProperty("user.dir") + FileSystems.getDefault().getSeparator() + "sitemapTest.txt";
        configuration.setTestSiteMapOutputFile(testFile);
        SiteMapFileWriter siteMapFileWriter = new SiteMapFileWriter(configuration);
        siteManager.addPageLinks(new PageWithLink("Site A", "Site AA"));
        siteManager.addPageLinks(new PageWithLink("Site A", "Site AB"));
        siteManager.addPageLinks(new PageWithLink("Site B", "Site BB"));
        //Add duplicate to ensure it is filtered out.
        siteManager.addPageLinks(new PageWithLink("Site B", "Site BB"));
        siteManager.addPageLinks(new PageWithLink("Site C", null));

        SiteMapBuilder siteMapBuilder = new SiteMapBuilder(siteManager, siteMapFileWriter);
        siteMapBuilder.growReport();
        siteMapBuilder.createSiteMap();

        String newFileContent = new String(Files.readAllBytes(Paths.get(testFile)));
        newFileContent = newFileContent.replaceAll("\\r\\n?", "\n");
        String expectedResult = "Site: Site A\n" +
                "\t - Site AA\n" +
                "\t - Site AB\n" +
                "Site: Site B\n" +
                "\t - Site BB\n" +
                "Site: Site C\n" +
                "\t - null\n";
        assertEquals("Check file is correctly Written", expectedResult, newFileContent);

    }



}