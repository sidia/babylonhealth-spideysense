package project.sitemap;

import org.junit.Test;
import project.configuration.ConfigValidator;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;

public class SiteMapFileWriterTest {

    @Test(expected = RuntimeException.class)
    public void failedWriteError() {
        TestConfig testConfig = new TestConfig(new ConfigValidator());
        testConfig.setTestSiteMapOutputFile("/\\");
        SiteMapFileWriter siteMapFileWriter = new SiteMapFileWriter(testConfig);
        Map<String, List<String>> testList = new HashMap<>();
        testList.put("T", Arrays.asList("T1", "T2"));
        siteMapFileWriter.write(testList);
    }

    @Test
    public void testEmptyList() throws IOException {
        String testFile = System.getProperty("user.dir") + FileSystems.getDefault().getSeparator() + "sitemapTestWriter.txt";
        TestConfig testConfig = new TestConfig(new ConfigValidator());
        testConfig.setTestSiteMapOutputFile(testFile);
        SiteMapFileWriter siteMapFileWriter = new SiteMapFileWriter(testConfig);
        Map<String, List<String>> testList = new HashMap<>();
        testList.put("T", Collections.emptyList());
        siteMapFileWriter.write(testList);


        String newFileContent = new String(Files.readAllBytes(Paths.get(testFile)));
        newFileContent = newFileContent.replaceAll("\\r\\n?", "\n");
        String expectedResult = "Site: T\n";
        assertEquals("Check file is correctly Written", expectedResult, newFileContent);

    }
}