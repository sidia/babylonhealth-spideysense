package project.sitemap;

import project.configuration.ConfigValidator;
import project.configuration.Configuration;

public class TestConfig extends Configuration {
    private boolean subDomain = true;
    private String testSiteMapOutputFile = super.getSiteMapOutputFile();

    public TestConfig(ConfigValidator configValidator) {
        super(configValidator);
    }
    @Override
    public String getSiteMapOutputFile() {
        return testSiteMapOutputFile;
    }

    @Override
    public boolean isIncludeSubDomains() {
        return subDomain;
    }

    public void setTestSiteMapOutputFile(String siteMapOutputFile){
        testSiteMapOutputFile = siteMapOutputFile;
    }

    public void setTestSubDomain(boolean value){
        subDomain = value;
    }
}
