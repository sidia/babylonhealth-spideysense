package project.crawler.input;

import org.junit.Test;

import static org.junit.Assert.*;

public class UrlValidatorTest {
    UrlValidator urlValidator = new UrlValidator();
    @Test
    public void testUrlValidator(){

        try {
            urlValidator.validate("http://www.google.com");
        } catch (InvalidInputException e) {
            fail();
        }
    }

    @Test(expected = InvalidInputException.class)
    public void testInvalidUrl(){
        urlValidator.validate("www.google.com");
    }
}