package project.crawler.input;

import org.junit.Test;
import project.configuration.ConfigValidator;
import project.sitemap.TestConfig;

import static org.junit.Assert.*;

public class InputCleanerTest {
    @Test
    public void testInputCleanerWithSubDomainsOn(){
        TestConfig configuration = new TestConfig(new ConfigValidator());
        InputCleaner inputCleaner = new InputCleaner(configuration);

        configuration.setTestSubDomain(true);
        assertEquals("Test https url", "test.com",
                inputCleaner.getSimpleDomain("https://test.com"));
        assertEquals("Test http url", "test.com",
                inputCleaner.getSimpleDomain("http://test.com"));
        assertEquals("Test www url", "test.com",
                inputCleaner.getSimpleDomain("http://www.test.com"));
        assertEquals("Test deeper url", "test.com/here",
                inputCleaner.getSimpleDomain("http://www.test.com/here"));

    }

    @Test
    public void testCleanerWithSubDomainOn(){
        TestConfig configuration = new TestConfig(new ConfigValidator());
        InputCleaner inputCleaner = new InputCleaner(configuration);
        configuration.setTestSubDomain(false);
        assertEquals("Test subdomain off", "http://www.test.com",
                inputCleaner.getSimpleDomain("http://www.test.com"));

    }

}