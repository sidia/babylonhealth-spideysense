package project.crawler.input;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InputHandlerTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test(expected = InvalidInputException.class)
    public void handleInputForInvalidNumberOfArguments() {
        InputHandler inputHandler = new InputHandler(new UrlValidator());
        inputHandler.handleInput(new String[]{"a", "b"});

    }

    @Test
    public void handleInput() {
        InputHandler inputHandler = new InputHandler(new UrlValidator());
        String result = inputHandler.handleInput(new String[]{"http://www.google.com"});
        assertEquals("Check result is 1st Argument", "http://www.google.com",result);

    }

}