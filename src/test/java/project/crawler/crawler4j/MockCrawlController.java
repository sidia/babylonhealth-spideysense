package project.crawler.crawler4j;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class MockCrawlController extends CrawlController {
    private boolean testFinished = super.finished;
    public MockCrawlController(CrawlConfig config, PageFetcher pageFetcher, RobotstxtServer robotstxtServer) throws Exception {
        super(config, pageFetcher, robotstxtServer);
    }

    @Override
    public boolean isFinished(){
        return testFinished;
    }

    public void setTestFinished(boolean state) {
        testFinished = state;
    }
}
