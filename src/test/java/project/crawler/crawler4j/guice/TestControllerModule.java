package project.crawler.crawler4j.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import project.configuration.ConfigValidator;
import project.configuration.Configuration;
import project.crawler.SiteManager;
import project.crawler.crawler4j.SpideyController;
import project.crawler.crawler4j.guice.MockCrawlControllerProvider;
import project.crawler.crawler4j.guice.MockTestConfigProvider;
import project.crawler.input.InputCleaner;
import project.crawler.input.InputHandler;
import project.guice.providers.PageFetcherProvider;
import project.guice.providers.RobotstxtServerProvider;

public class TestControllerModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ConfigValidator.class);
        bind(Configuration.class).toProvider(MockTestConfigProvider.class).in(Singleton.class);
        bind(InputHandler.class);
        bind(SiteManager.class).in(Singleton.class);
        bind(InputCleaner.class);
        bind(RobotstxtServer.class).toProvider(RobotstxtServerProvider.class);
        bind(PageFetcher.class).toProvider(PageFetcherProvider.class);
        bind(CrawlController.class).toProvider(MockCrawlControllerProvider.class).in(Singleton.class);
        bind(SpideyController.class);
    }
}
