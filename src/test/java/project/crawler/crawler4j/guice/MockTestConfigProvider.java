package project.crawler.crawler4j.guice;

import com.google.inject.Provider;
import project.configuration.ConfigValidator;
import project.configuration.Configuration;
import project.sitemap.TestConfig;

public class MockTestConfigProvider implements Provider<Configuration> {
    @Override
    public Configuration get() {
        return new TestConfig(new ConfigValidator());
    }
}
