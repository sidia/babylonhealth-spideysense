package project.crawler.crawler4j.guice;

import com.google.inject.Inject;
import com.google.inject.Provider;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import project.configuration.Configuration;
import project.crawler.crawler4j.MockCrawlController;

public class MockCrawlControllerProvider implements Provider<CrawlController> {

    private final Configuration configuration;
    private final PageFetcher pageFetcher;
    private final RobotstxtServer robotstxtServer;

    @Inject
    MockCrawlControllerProvider(Configuration configuration, PageFetcher pageFetcher, RobotstxtServer robotstxtServer){
        this.pageFetcher = pageFetcher;
        this.configuration = configuration;
        this.robotstxtServer = robotstxtServer;
    }

    @Override
    public CrawlController get() {
        try {
            return new MockCrawlController(configuration.getCrawlConfig(), pageFetcher, robotstxtServer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
