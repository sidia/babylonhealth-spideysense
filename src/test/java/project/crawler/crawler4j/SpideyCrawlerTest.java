package project.crawler.crawler4j;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;
import org.junit.Test;
import project.crawler.SiteManager;
import project.crawler.crawler4j.SpideyCrawler;

import static org.junit.Assert.*;

public class SpideyCrawlerTest {
    @Test
    public void testValidCrawlerInput(){
        SpideyCrawler spideyCrawler = new SpideyCrawler("test.com",new SiteManager());
        WebURL pageUrl = new WebURL();
        pageUrl.setURL("http://test.com");
        Page page = new Page(pageUrl);
        WebURL linkUrl = new WebURL();
        linkUrl.setURL("http://one.test.com");
        assertTrue("http://one.test.com is a valid url",spideyCrawler.shouldVisit(page, linkUrl));
    }

    @Test
    public void testValidCrawlerInputDueToFile(){
        SpideyCrawler spideyCrawler = new SpideyCrawler("test.com",new SiteManager());
        WebURL pageUrl = new WebURL();
        pageUrl.setURL("http://test.com");
        Page page = new Page(pageUrl);
        WebURL linkUrl = new WebURL();
        linkUrl.setURL("http://one.test.com/a.svg");
        assertFalse("http://test.com/a.svg is an invalid url",spideyCrawler.shouldVisit(page, linkUrl));
    }

    @Test
    public void testValidCrawlerInputDueToNonDomain(){
        SpideyCrawler spideyCrawler = new SpideyCrawler("test.com",new SiteManager());
        WebURL pageUrl = new WebURL();
        pageUrl.setURL("http://test.com");
        Page page = new Page(pageUrl);
        WebURL linkUrl = new WebURL();
        linkUrl.setURL("http://google.com");
        assertFalse("http://google.com is an invalid url",spideyCrawler.shouldVisit(page, linkUrl));
    }

}