package project.crawler.crawler4j;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import org.junit.Test;
import project.crawler.crawler4j.guice.TestControllerModule;

public class SpideyControllerTest {
    @Test
    public void testFlowOfController() throws Exception {
        Injector in = Guice.createInjector(new TestControllerModule());
        SpideyController spideyController = in.getInstance(SpideyController.class);
        MockCrawlController crawler = (MockCrawlController) in.getInstance(CrawlController.class);
        spideyController.crawl("http://www.google.com");
        do {
            Thread.sleep(1000);
            crawler.setTestFinished(true);
        } while (spideyController.isCrawling());
    }
}