package project.sitemap;

import com.google.inject.Inject;

import org.apache.commons.io.FileUtils;
import project.configuration.Configuration;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;


/**
 * This will take a SiteMap and produce a file based on configuration provided.
 */
public class SiteMapFileWriter implements SiteMapWriter{
    private final Configuration config;

    @Inject
    SiteMapFileWriter(Configuration configuration) {
        this.config = configuration;
    }

    /**
     * Writes out the file from the map provided.
     * @param siteData map of data
     */
    @Override
    public void write(Map<String, List<String>> siteData) {
        StringBuilder sb = new StringBuilder();
        siteData.forEach((k, v) -> {
            sb.append("Site: ").append(k).append(System.lineSeparator());
            if (v.size() > 0) {
                v.forEach(u -> sb.append("\t").append(" - ").append(u).append(System.lineSeparator()));
            }
        });
        try{
            FileUtils.writeStringToFile(new File(config.getSiteMapOutputFile()),sb.toString(), Charset.defaultCharset());
            System.out.println("Your SiteMap file has been created successfully!");
            System.out.println(String.format("You can find your SiteMap file at [%s]", config.getSiteMapOutputFile()));
        } catch (IOException e) {
            throw new RuntimeException("ERROR: Unable to produce file due to Exception", e);
        }
    }
}
