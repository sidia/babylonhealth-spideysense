package project.sitemap;

import com.google.inject.Inject;
import project.crawler.dto.PageWithLink;
import project.crawler.SiteManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the SiteMapBuilder, it will use the SiteManager's data and a SiteMapWriter to produce the SiteMap.
 */
public class SiteMapBuilder {
    private final SiteManager siteManger;
    private final SiteMapWriter siteMapWriter;
    Map<String, List<String>> urlMap = new HashMap<>();

    @Inject
    SiteMapBuilder(SiteManager siteManager,
                   SiteMapWriter siteMapWriter) {
        this.siteManger = siteManager;
        this.siteMapWriter = siteMapWriter;
    }

    /**
     * This will start working on items in the queue and continue until the queue is empty. Items in the queue
     * Are placed in the MAP which will contain the SiteMap data.
     */
    public void growReport() {
        while (!siteManger.pagesWithLinksIsEmpty()){
            PageWithLink pageWithLink = siteManger.pullNextMustVisitPage();
            List<String> list;
            if (urlMap.containsKey(pageWithLink.getPage())){
                list = urlMap.get(pageWithLink.getPage());
                if (!list.contains(pageWithLink.getLink())) list.add(pageWithLink.getLink());
            }else{
                list = new ArrayList<>();
                urlMap.put(pageWithLink.getPage(), list);
                list.add(pageWithLink.getLink());
            }

        }
    }

    /**
     * This creates the sitemap based on what SiteMapWriter was injected.
     */
    public void createSiteMap() {
        siteMapWriter.write(urlMap);
    }
}
