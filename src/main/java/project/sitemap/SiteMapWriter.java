package project.sitemap;

import java.util.List;
import java.util.Map;

public interface SiteMapWriter {
    void write(Map<String, List<String>> siteData);
}
