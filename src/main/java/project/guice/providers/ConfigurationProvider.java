package project.guice.providers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import project.configuration.ConfigValidator;
import project.configuration.Configuration;

/** Provider for Configuration Injection **/
public class ConfigurationProvider implements Provider<Configuration> {
    private final ConfigValidator configValidator;

    @Inject
    public ConfigurationProvider(ConfigValidator configValidator){
        this.configValidator = configValidator;

    }

    @Override
    public Configuration get() {
        return new Configuration(configValidator);
    }
}
