package project.guice.providers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;

/** Provider for PageFetcher Injection **/
public class PageFetcherProvider implements Provider<PageFetcher> {
    private final PageFetcher pageFetcher;

    @Inject
    public PageFetcherProvider(CrawlConfig config) {
        this.pageFetcher = new PageFetcher(config);

    }
    @Override
    public PageFetcher get() {
        return pageFetcher;
    }
}
