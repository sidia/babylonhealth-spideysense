package project.guice.providers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import project.configuration.Configuration;


/** Provider for CrawlController Injection **/
public class CrawlControllerProvider implements Provider<CrawlController> {
    private final CrawlController crawlController;

    @Inject
    public CrawlControllerProvider(Configuration config, PageFetcher pageFetcher, RobotstxtServer robotstxtServer) throws Exception {
        this.crawlController = new CrawlController(config.getCrawlConfig(), pageFetcher, robotstxtServer);
    }

    @Override
    public CrawlController get() {
        return crawlController;
    }
}
