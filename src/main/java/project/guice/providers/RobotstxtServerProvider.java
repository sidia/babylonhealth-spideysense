package project.guice.providers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/** Provider for RobotstxtServer Injection **/
public class RobotstxtServerProvider implements Provider<RobotstxtServer> {
    private final RobotstxtServer robotstxtServer;

    @Inject
    public RobotstxtServerProvider(RobotstxtConfig robotstxtConfig, PageFetcher pageFetcher) {
        this.robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
    }
    @Override
    public RobotstxtServer get() {
        return robotstxtServer;
    }
}
