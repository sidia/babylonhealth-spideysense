package project.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import project.configuration.ConfigValidator;
import project.configuration.Configuration;
import project.crawler.input.InputCleaner;
import project.crawler.crawler4j.SpideyController;
import project.crawler.SiteManager;

import project.crawler.input.InputHandler;
import project.crawler.input.UrlValidator;
import project.guice.providers.ConfigurationProvider;
import project.guice.providers.CrawlControllerProvider;
import project.guice.providers.PageFetcherProvider;
import project.guice.providers.RobotstxtServerProvider;
import project.sitemap.SiteMapFileWriter;
import project.sitemap.SiteMapWriter;


/**
 * Main application base module for Injection
 */
public class SpideyBaseModule extends AbstractModule {

    @Override
    protected void configure() {
        /* Configuration */
        bind(UrlValidator.class);
        bind(ConfigValidator.class);
        bind(Configuration.class).toProvider(ConfigurationProvider.class).in(Singleton.class);

        /* Writers */
        bind(SiteMapWriter.class).to(SiteMapFileWriter.class);

        /* Input */
        bind(InputHandler.class);
        bind(InputCleaner.class);

        /*State */
        bind(SiteManager.class).in(Singleton.class);

        /* Crawl4J */
        bind(RobotstxtServer.class).toProvider(RobotstxtServerProvider.class);
        bind(PageFetcher.class).toProvider(PageFetcherProvider.class);
        bind(CrawlController.class).toProvider(CrawlControllerProvider.class);
        bind(SpideyController.class);
    }
}

