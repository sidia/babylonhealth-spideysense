package project.crawler.crawler4j;

import com.google.inject.Inject;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import project.configuration.Configuration;
import project.crawler.SiteManager;
import project.crawler.input.InputCleaner;

/**
 * The SpideyController will create a number of crawlers as specified in the Configuration. The crawlers will process
 * in a non-blocking way this is to allow the main thread to continue processing items that were put on the queue by
 * the Crawlers
 */
public class SpideyController {
    private final CrawlController crawlController;
    private final InputCleaner inputCleaner;
    private final SiteManager siteManager;
    private final Configuration config;

    @Inject
    public SpideyController(Configuration config, InputCleaner inputCleaner, CrawlController crawlController,
                            SiteManager siteManager) {
        this.config = config;
        this.siteManager = siteManager;
        this.inputCleaner = inputCleaner;
        this.crawlController = crawlController;
    }

    /**
     * Runs the crawl action and spawns all Crawlers. Will also use the inputCleaner to ensure correct url is passed
     * to crawlers based on configuration.
     * @param url passed in by the user
     */
    public void crawl(String url) {
        crawlController.addSeed(url);
        CrawlController.WebCrawlerFactory<SpideyCrawler> factory = () ->
                new SpideyCrawler(inputCleaner.getSimpleDomain(url), siteManager);
        crawlController.startNonBlocking(factory, config.getNumberOfCrawlers());
    }

    /**
     * Checks if we are still Crawling, if we are not it calls the shutdown method as well and returns the end state.
     */
    public boolean isCrawling() {
        if (crawlController.isFinished()) {
            crawlController.shutdown();
        }
        return !crawlController.isShuttingDown();
    }
}
