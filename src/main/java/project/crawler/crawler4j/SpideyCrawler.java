package project.crawler.crawler4j;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.url.WebURL;
import project.crawler.dto.PageWithLink;
import project.crawler.SiteManager;

import java.util.regex.Pattern;

/**
 * This is the Crawler extended from WebCrawler in Crawler4J. It will determine if we care about a link when using
 * the shouldVisit method.
 */
public class SpideyCrawler extends WebCrawler {
    private final static Pattern FILTERS = Pattern.compile(".*(\\.(ico|svg|css|js|gif|jpg|png|mp3|mp4|zip|gz))$");

    private final Pattern VALID_DOMAIN;
    private final SiteManager siteManager;

    SpideyCrawler(String domain, SiteManager siteManager){
        this.siteManager = siteManager;
        this.VALID_DOMAIN = Pattern.compile(".*" + domain + ".*");
    }


    /**
     * We use this to determine if we need to track the url. If it needs to be tracked it will place the referringPage
     * and the url into the PageWithLink DTO on the Queue in the SiteManager.
     * @param referringPage page that we scanned for sites
     * @param url url found on page
     * @return true if we should visit this page as well.
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        String href = url.getURL().toLowerCase();
        String page = referringPage.getWebURL().getURL().toLowerCase();
        boolean shouldVisit = !FILTERS.matcher(href).matches()
                && (VALID_DOMAIN.matcher(href).matches());
        if (shouldVisit) {
            String refUrl = page.split("\\?")[0];
            String linkUrl = href.split("\\?")[0];
            siteManager.addPageLinks(new PageWithLink(refUrl,linkUrl));
        }
        return shouldVisit;
    }
}
