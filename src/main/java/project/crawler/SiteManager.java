package project.crawler;

import project.crawler.dto.PageWithLink;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * The SiteManager is responsible for managing the crawler feedback on possible links. It uses the PagesWithLink DTO
 * to carry the Page and Link.
 */
public class SiteManager {
    private Queue<PageWithLink> pagesWithLinks = new ConcurrentLinkedDeque<>();

    /**
     * Adds a PageWithLink safely to the queue
     * @param pageWithLink item to store
     */
    public void addPageLinks(PageWithLink pageWithLink) {
        pagesWithLinks.add(pageWithLink);
    }

    /**
     * Returns true of the queue is empty.
     */
    public boolean pagesWithLinksIsEmpty(){
        return pagesWithLinks.isEmpty();
    }

    /**
     * Provides an Item from the queue to the caller.
     */
    public PageWithLink pullNextMustVisitPage() {
        return pagesWithLinks.poll();
    }

}
