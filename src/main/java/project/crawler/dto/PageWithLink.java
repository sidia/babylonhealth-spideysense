package project.crawler.dto;

/**
 * This DTO object is used to pass around the page and a single link throughout the system.
 */
public class PageWithLink {
    private final String page;
    private final String link;

    public PageWithLink(String page, String link) {
        this.page = page;
        this.link = link;
    }

    public String getPage() {
        return page;
    }

    public String getLink() {
        return link;
    }
}
