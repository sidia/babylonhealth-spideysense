package project.crawler.input;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * This validates the url provided by the user.
 */
public class UrlValidator {
    void validate(String input){
        try {
            new URL(input);
        } catch (MalformedURLException e) {
            throw new InvalidInputException(String.format("Url is not valid! Provided [%s] " +
                    "Expected example: [http://www.google.com]", input),e);
        }

    }
}
