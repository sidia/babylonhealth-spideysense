package project.crawler.input;

import com.google.inject.Inject;
import project.configuration.Configuration;

/**
 * This assists in checking any urls based on configuration of IncludeSubDomain in the configuration class.
 */
public class InputCleaner {

    private final Configuration configuration;
    @Inject
    InputCleaner(Configuration configuration){
        this.configuration = configuration;
    }

    /**
     * Returns a simpler version of the url if the configuration is set. Otherwise it just returns the url as is.
     * @param url to be modified / kept
     */
    public String getSimpleDomain(String url){
        String cleanUrl = url;
        if (configuration.isIncludeSubDomains()){
            cleanUrl = url.replaceAll("^https?://(www.)?","");
        }
        return cleanUrl;
    }
}
