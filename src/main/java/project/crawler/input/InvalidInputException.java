package project.crawler.input;

/**
 * This exception is used for invalid user input.
 */
class InvalidInputException extends RuntimeException {
    InvalidInputException(String s) {
        super(s);
    }
    InvalidInputException(String s, Exception e) {
        super(s,e);
    }
}
