package project.crawler.input;

import com.google.inject.Inject;

/**
 * This handles input from the main application, it will throw an InvalidInputException if the input provided is not
 * as expected.
 */
public class InputHandler {
    private final UrlValidator urlValidator;

    @Inject
    InputHandler(UrlValidator urlValidator){
        this.urlValidator = urlValidator;

    }

    /**
     * Runs all input validations and returns a String back to the application.
     * @param args from the main application
     */
    public String handleInput(String[] args){
        if (args.length != 1){
            throw new InvalidInputException(
                    String.format("Invalid number of arguments. Provided [%d], expecting [1].", args.length));
        }
        urlValidator.validate(args[0]);
        return args[0];
    }
}
