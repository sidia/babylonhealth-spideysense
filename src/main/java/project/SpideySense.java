package project;

import com.google.inject.Guice;
import com.google.inject.Injector;
import project.crawler.crawler4j.SpideyController;
import project.crawler.input.InputHandler;
import project.guice.SpideyBaseModule;
import project.sitemap.SiteMapBuilder;

import java.util.concurrent.TimeUnit;

/**
 * Main application
 */
public class SpideySense {
    public static void main(String[] args) {
        Injector in = Guice.createInjector(new SpideyBaseModule());
        in.getInstance(SpideySense.class).run(args, in);
    }

    /**
     * Runs the crawlers
     * @param args arguments passed into the main application
     * @param injector the guice injector
     */
    public void run(String[] args, Injector injector){
        long start = System.currentTimeMillis();
        InputHandler inputHandler = injector.getInstance(InputHandler.class);
        String url = inputHandler.handleInput(args);

        SpideyController spideyController = injector.getInstance(SpideyController.class);
        spideyController.crawl(url);

        SiteMapBuilder sitemapBuilder = injector.getInstance(SiteMapBuilder.class);
        do {
            sitemapBuilder.growReport();
        } while (spideyController.isCrawling());
        sitemapBuilder.createSiteMap();
        System.out.println("Took: " +
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - start) + " Seconds");

        /*
         * Due to a bug on crawl4j - https://github.com/yasserg/crawler4j/issues/327
         */
        System.exit(0);
    }
}
