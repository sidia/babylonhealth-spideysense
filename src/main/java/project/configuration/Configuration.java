package project.configuration;

import com.google.inject.Inject;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;

import java.nio.file.FileSystems;

/**
 * This is the configuration class that will injected as a Singleton. This config's content should be enhanced to read
 * all values from a file.
 *
 */
public class Configuration {
    /** This is the number of Crawler Threads to spawn - used by Crawler4J **/
    private final int numberOfCrawlers = 10;

    /** This is where the Crawlers will write internal data as required by Crawler4J **/
    private final String crawlStorageFolder = System.getProperty("user.dir");

    /** This is the folder we will use to send the SiteMap output to **/
    private final String siteMapOutputFolder = crawlStorageFolder;

    /** This is the filename of the SiteMap**/
    private final String siteMapOutputFile = siteMapOutputFolder + FileSystems.getDefault().getSeparator() + "sitemap.txt";

    /**
     * This will enable/disable deeper search functionality.
     * Example: If the user input is [http://www.babylonhealth.com] and this value is enabled, we will change the domain
     * search to be it will match also match on domains [http://www.support.babylonhealth.com].
     */
    private final boolean includeSubDomains = false;

    /** Config of the crawlers used by Crawler4J **/
    private final CrawlConfig crawlConfig = new CrawlConfig();

    /**
     * Returns the number of Crawlers to be spawned.
     */
    public int getNumberOfCrawlers() {
        return numberOfCrawlers;
    }

    /**
     * Returns the CrawlStorageFolder location
     */
    public String getCrawlStorageFolder() {
        return crawlStorageFolder;
    }

    /**
     * Returns the SiteMapOutputFolder location
     */
    public String getSiteMapOutputFolder() {
        return siteMapOutputFolder;
    }

    /**
     * Returns the SiteMap output file name
     */
    public String getSiteMapOutputFile() {
        return siteMapOutputFile;
    }

    /**
     * Returns value of includeSubDomains boolean value
     */
    public boolean isIncludeSubDomains() {
        return includeSubDomains;
    }

    /**
     * Returns CrawlConfig
     */
    public CrawlConfig getCrawlConfig() {
        return crawlConfig;
    }

    /**
     * This constructor will also validate the SiteOutput directory.
     * @param configValidator validator for certain configurations.
     */
    @Inject
    public Configuration(ConfigValidator configValidator){
        crawlConfig.setCrawlStorageFolder(crawlStorageFolder);
        crawlConfig.setMaxDepthOfCrawling(5);
        crawlConfig.setMaxConnectionsPerHost(numberOfCrawlers);
        crawlConfig.setFollowRedirects(false);
        crawlConfig.setRespectNoIndex(true);
        crawlConfig.setRespectNoFollow(true);
        crawlConfig.setResumableCrawling(false);
        crawlConfig.setIncludeBinaryContentInCrawling(false);
        crawlConfig.setProcessBinaryContentInCrawling(false);
        configValidator.validateDirectory("SiteMap Output Directory", siteMapOutputFolder);
        configValidator.validateDirectory("CrawlStorage Folder", crawlStorageFolder);
    }
}
