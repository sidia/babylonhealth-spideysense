package project.configuration;

/**
 * This exception is used for invalid configuration.
 */
class InvalidConfigurationException extends RuntimeException{
    InvalidConfigurationException(String s){
        super(s);
    }
}
