package project.configuration;

import java.io.File;

/**
 * This validator will assist in confirming config is correct before starting the application. It will throw a Runtime
 * Exception of type InvalidConfigurationException.
 */
public class ConfigValidator {

    /**
     * This validates directories are writable for the application.
     * @param configName name of the Configuration to be checked
     * @param dir the directory to check
     */
    void validateDirectory(String configName, String dir) {
        File checkDir = new File(dir);
        if (!checkDir.canWrite()){
            throw new InvalidConfigurationException(
                    String.format("Invalid configuration for [%s], You unable to write to directory!",configName));
        }
    }
}
