Forethought
===========
This has been the most interesting challenge for me personally. Probably due to my lack of exposure to **WWW** and **Crawler** projects. While doing research on Crawlers and what can go wrong... I stumbled across the site [crawler-test.com](https://www.crawler-test.com/). This site was obviously designed to instill a great deal of respect for **"what can go wrong"** when building a Web Crawler. I really enjoyed this challenge. Thank you :)


Decisions
=========
a) Libs
---------
I have selected the following libraries mostly due to the lack of past experience in the **WebCrawler** as well as **WWW** domain. These domains have multiple layers of complexity that I found by just looking at the most common testing patterns. I have also selected some libraries due to familiarity to reduce the required time spent.
- [Crawler4J](https://github.com/yasserg/crawler4j) will be used mostly to cover my lack of domain knowledge and the time limitations.
- [Google Guice](https://github.com/google/guice) will be used due to past usage experience.


b) Design
---------
The **Crawler4J** library will provide the **shouldVisit** method. When this method is called we have access to both the **parent** page as well as the **child** page. This information will be pushed to a queue to protect against concurrency. The main thread will be reading this queue and build a map with all the SiteMap data finish. When the crawlers have completed their work, we will build a **TBD** from the list and use it to print out the map.


c) Configuration
----------------
The configuration at the moment is hard-coded, ideally this should be read from a file and packaged with the application. Depending on the layout this might also make sense if it was placed in a team central Util.


d) Logging
----------
The logging of the application can be improved vastly (And should be sourced from a central Util). Due to time constraints I have focused on providing the **"version 1"** of the application.


e) Suggested Improvements
-------------------------
- The code from Crawl4J can be replaced to be more focused on business requirements. This will be determined by more detailed requirements.
- Dockerize the application for easier use.
- Implement application configuration loading from File.
- Implement logging strategy.


Running and Building on WINDOWS
===============================
To build your app:
- Run gradlew.bat clean build

To build a distribution:
- Run gradlew.bat installDist

To run a newly built application: *NOTE: Depends on you building the distribution*
- cd {source.dir}\build\install\SpideySense
- bin\SpideySense.bat https://www.babylonhealth.com/

Run previously built application:
- Extract the SpideySense-1.0.zip into your {deploy-folder}
- cd {deploy-folder}\SpideySense-1.0
- bin\SpideySense.bat


Running and Building on Linux/Unix
==================================
To build your app:
- Run ./gradlew clean build

To build a distribution:
- Run ./gradlew installDist

To run a newly built application: *NOTE: Depends on you building the distribution*
- cd {source.dir}/build/install/SpideySense
- bin/SpideySense https://www.babylonhealth.com/

Run previously built application:
- Extract the SpideySense-1.0.zip into your {deploy-folder}
- cd {deploy-folder}/SpideySense-1.0
- bin/SpideySense

*NOTE Application will produce a SiteMap.txt file in the location where the app is run from.*

